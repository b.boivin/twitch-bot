const { getStream } = require('./twitch.js');
const { liveChannelId, guildId } = require('../config.json');
const { EmbedBuilder } = require('discord.js');
const fs = require('node:fs');

function replaceData(str, data) {
  if (!str) return str;
  let newStr = str;
  const dataCopy = { ...data };
  Object.keys(dataCopy).forEach((key) => {
    newStr = newStr.replace(`{{${key}}}`, (dataCopy[key]+'').replace('{width}', '426').replace('{height}', '240'));
  });
  return newStr;
}

function buildEmbed(embedJson, streamData) {
  const embed = new EmbedBuilder();
  if (embedJson.color) {
    embed.setColor(embedJson.color);
  }
  if (embedJson.title) {
    embed.setTitle(replaceData(embedJson.title, streamData));
  }
  if (embedJson.url) {
    embed.setURL(replaceData(embedJson.url, streamData));
  }
  if (embedJson.author) {
    embed.setAuthor({
      name : replaceData(embedJson.author?.name, streamData),
      url : replaceData(embedJson.author?.url, streamData),
      iconURL : replaceData(embedJson.author?.iconUrl, streamData),
    });
  }
  if (embedJson.description) {
    embed.setDescription(replaceData(embedJson.description, streamData));
  }
  if (embedJson.thumbnail) {
    embed.setThumbnail(replaceData(embedJson.thumbnail, streamData));
  }
  if (embedJson.image) {
    embed.setImage(replaceData(embedJson.image, streamData));
  }
  if (embedJson.footer) {
    embed.setFooter({
      text : replaceData(embedJson.footer?.text, streamData),
      iconUrl : replaceData(embedJson.footer?.iconUrl, streamData),
    });
  }

  if (embedJson.fields?.length) {
    for (const field of embedJson.fields) {
      embed.addFields({
        name : replaceData(field.name, streamData),
        value : replaceData(field.value, streamData),
        inline : field.inline,
      });
    }
  }

  embed.setTimestamp();
  return embed;
}

module.exports = {
  async execute(client, username, embedJson) {
    const stream = await getStream(username);
    let lastMessage = undefined;
    if (fs.existsSync(`./json/lastLive${username}.json`)) {
      const savedJson = fs.readFileSync(`./json/lastLive${username}.json`, 'utf8');
      lastMessage = JSON.parse(savedJson)?.messageId;
    }
    if (stream.data?.length && stream.data[0].id !== lastMessage) {
      const guild = client.guilds.cache.get(guildId);
      const liveChannel = await guild.channels.fetch(liveChannelId);
      const streamData = stream.data[0];
      const embed = buildEmbed(embedJson, streamData);
      await liveChannel.send({ content: '@everyone', embeds: [embed]});
      fs.writeFileSync(`./json/lastLive${username}.json`, JSON.stringify({messageId: streamData.id}));
    }
  },
}
