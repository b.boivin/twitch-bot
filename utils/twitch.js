const { twitchSecretId, twitchClientId } = require('../config.json');
const fetch = require('node-fetch');

async function getHeaders() {
  const tokenJson = await fetch(`https://id.twitch.tv/oauth2/token?client_id=${twitchClientId}&client_secret=${twitchSecretId}&grant_type=client_credentials`, { method: "POST" }).then((res) => res.json());
  const token = tokenJson.access_token;
  return {
    "Client-ID": twitchClientId,
    Authorization: `Bearer ${token}`,
  }
}

module.exports = {
  async getFollowers(userId) {
    const headers = await getHeaders();
    return fetch(`https://api.twitch.tv/helix/users/follows?to_id=${userId}`, { headers }).then((res) => res.json());
  },
  async getStream(username) {
    const headers = await getHeaders();
    const data = await fetch(`https://api.twitch.tv/helix/streams?user_login=${username}`, { headers }).then((res) => res.json());
    const profile = await fetch(`https://api.twitch.tv/helix/users?login=${username}`, { headers }).then((res) => res.json());
    if (data?.data?.length) {
      data.data[0].profile_image_url = profile.data?.[0]?.profile_image_url;
    }
    return data;
  }
};
