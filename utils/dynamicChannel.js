const { guildId } = require("../config.json");
const fs = require("node:fs");
const { ChannelType } = require("discord.js");

module.exports = {
  async execute(name, id, oldState, newState) {
    let voice = [id];
    if (fs.existsSync(`./json/dynamic${name}.json`)) {
      const savedJson = fs.readFileSync(`./json/dynamic${name}.json`, 'utf8');
      voice = JSON.parse(savedJson);
    }

    const client = oldState?.channel?.client || newState?.channel?.client;
    const guild = client.guilds.cache.get(guildId);
    const category = guild.channels.cache.get(id).parent;
    const [oldId, newId] = [oldState.channelId, newState.channelId];
    if (voice.includes(newId)) {
      const isFull = voice.reduce((acc, it) => {
        return acc && !!guild.channels.cache.get(it)?.members.size;
      }, true);
      if (isFull) {
        const newChan = await guild
          .channels.create({
            name: `🎤 -ton-salon-${voice.length + 1}-🎤`,
            type: ChannelType.GuildVoice,
            parent: category,
          });
        await newChan.setPosition(newChan.position - 1);
        voice.push(newChan.id);
        fs.writeFileSync(`./json/dynamic${name}.json`, JSON.stringify(voice));
      }
    }
    if (voice.includes(oldId)) {
      const isFull = voice.reduce((acc, it) => {
        if (it === oldId) return true;
        return acc && !!guild.channels.cache.get(it)?.members.size;
      }, true);
      const isEmpty = voice.reduce((acc, it) => {
        return acc && !guild.channels.cache.get(it)?.members.size;
      }, true);
      if (isEmpty) {
        for (const channelId of voice) {
          if (channelId !== voice[0]) {
            await guild.channels.cache.get(channelId).delete("empty");
            voice = voice.filter((it) => it !== channelId);
            fs.writeFileSync(
              `./json/dynamic${name}.json`,
              JSON.stringify(voice)
            );
          }
        }
      } else if (!isFull) {
        const toDelete = voice.filter(
          (it) => !guild.channels.cache.get(it).members.size
        );
        for (const channelId of toDelete) {
          if (channelId !== toDelete[0]) {
            await guild.channels.cache.get(channelId).delete("empty");
            voice = voice.filter((it) => it !== channelId);
            fs.writeFileSync(
              `./json/dynamic${name}.json`,
              JSON.stringify(voice)
            );
          }
        }
      }
    }
  },
};
