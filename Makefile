init:
		docker build -t askenironbot .
dev:
		docker run -it -v $(pwd):/app askenironbot
update:
		git pull
		docker build -t askenironbot .
run:
		docker run -d --name askenironbot --restart unless-stopped askenironbot