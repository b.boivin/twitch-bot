const { Client, Collection, GatewayIntentBits, Events } = require("discord.js");
const fs = require("node:fs");
const path = require("node:path");
const { token } = require("./config.json");

const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildVoiceStates,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildBans,
    GatewayIntentBits.GuildMembers,
  ],
});

client.commands = new Collection();
const commandFiles = fs
  .readdirSync("./commands")
  .filter((file) => file.endsWith(".js"));

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  client.commands.set(command.data.name, command);
}


const eventFiles = fs
  .readdirSync("./events")
  .filter((file) => file.endsWith(".js"));

for (const file of eventFiles) {
  const event = require(`./events/${file}`);
  if (event.once) {
    client.once(event.name, (...args) => event.execute(...args));
  } else {
    client.on(event.name, (...args) => event.execute(...args));
  }
}

client.on('ready', () => {
  const intervalFiles = fs
    .readdirSync("./intervals")
    .filter((file) => file.endsWith(".js"));
  
  for (const file of intervalFiles) {
    const interval = require(`./intervals/${file}`);
    if (!interval.time || !interval.execute) continue;
    if (!interval.delayed) {
      try {
        interval.execute(client);
      } catch (error) {
        console.error(error);
      }
    }
    setInterval(() => {
      try {
        interval.execute(client)
      } catch (error) {
        console.error(error);
      }
    }, interval.time);
    console.log(`Interval ${file.replace('.js', '')} loaded`);
  }
})


client.login(token);
