const { ActionRowBuilder, ButtonBuilder, ButtonStyle } = require("discord.js");
const { guildId, roleChannelId } = require('../config.json');
const fs = require('node:fs');

module.exports = {
  name: "ready",
  async execute(client) {
    const guild = client.guilds.cache.get(guildId);

    // Refresh the select streamers button
    const roleChannel = guild.channels.cache.get(roleChannelId);
    const messagesMap = await roleChannel.messages.fetch()
    const messages = Array.from(messagesMap, ([key, value]) => {
      return {...value, id: key};
    }).sort((a, b) => b.createdTimestamp - a.createdTimestamp);
    const lastMessageId = messages[0]?.id;

    let lastBotMessage = undefined;
    if (fs.existsSync('./json/lastBotMessage.json')) {
      const savedJson = fs.readFileSync('./json/lastBotMessage.json', 'utf8');
      lastBotMessage = JSON.parse(savedJson)?.streambtn;
    }

    if (!lastMessageId || !lastBotMessage || lastBotMessage !== lastMessageId) {
      if (lastBotMessage) {
        const toDelete = await roleChannel.messages.fetch(lastBotMessage).catch(() => null);
        if (toDelete)
          await toDelete.delete();
      }
      const row = new ActionRowBuilder()
			.addComponents(
				new ButtonBuilder()
					.setCustomId('follow')
					.setLabel('Choisir mes streamers')
					.setStyle(ButtonStyle.Primary),
			)
      const message = await roleChannel.send({ content: 'Pour choisir les streamers que tu follow clique sur ce bouton :\n(En choisissant un streamer, je reconnais avoir lu et accepté le réglement)', components: [row] });
      fs.writeFileSync('./json/lastBotMessage.json', JSON.stringify({streambtn: message.id}));
    }
  },
};
