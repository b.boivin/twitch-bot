const fs = require('node:fs');

module.exports = {
  name: "voiceStateUpdate",
  async execute(oldState, newState) {
    if (!oldState.channel) return;

    // Get all custom channels
    let voice = [];
    if (fs.existsSync(`./json/dynamicvoice.json`)) {
      const savedJson = fs.readFileSync(`./json/dynamicvoice.json`, 'utf8');
      voice = JSON.parse(savedJson);
    }

    if(!oldState.channel.members.size && voice.includes(oldState.channel.id)) {
      await new Promise(resolve => setTimeout(resolve, 400));
      if (fs.existsSync(`./json/dynamicvoice.json`)) {
        const savedJson = fs.readFileSync(`./json/dynamicvoice.json`, 'utf8');
        voice = JSON.parse(savedJson);
      }
      const guild = oldState.channel.guild;
      const customChannels = [];
      for (const channelId of voice) {
        customChannels.push(await guild.channels.fetch(channelId));
      }
      let availableNames = Array(voice.length).fill(0).map((_, i) => i ? `🎤-ton-salon-${i + 1}-🎤` : '🎤-ton-salon-🎤').filter(name => !customChannels.some(channel => channel.name === name));
      if (voice.length === 1) availableNames = ['🎤-ton-salon-🎤'];
      if (oldState.channel.name !== availableNames?.[0] || `🎤-ton-salon-${voice.length + 1}-🎤`)
        oldState.channel.setName(availableNames?.[0] || `🎤-ton-salon-${voice.length + 1}-🎤`);
    }

  }
};
