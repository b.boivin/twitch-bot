const { execute } = require('../utils/dynamicChannel.js');

module.exports = {
  name: "voiceStateUpdate",
  async execute(oldState, newState) {
    await execute('voice', '1036573616625700867', oldState, newState);
  }
};