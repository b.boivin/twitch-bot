module.exports = {
  name: "interactionCreate",
  async execute(interaction) {
    if (!interaction.isCommand() && !(interaction.isStringSelectMenu() || interaction.isButton())) return;
    const client = interaction.client;

    const name = interaction.isCommand()
      ? interaction.commandName
      : interaction.customId;

    const command = client.commands.get(name);

    if (!command) return;

    try {
      await command.execute(interaction);
    } catch (error) {
      console.error(error);
      await interaction.reply({
        content: "Il y a eu une erreur lors de l'execution de cette commande !",
        ephemeral: true,
      });
    }
  },
};
