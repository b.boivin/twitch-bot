FROM node:16
WORKDIR /app
COPY package.json /app/
RUN npm install
COPY . /app/
RUN mkdir -p /app/json
RUN npm run deploy
CMD ["npm", "start"]