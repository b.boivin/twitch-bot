const { execute } = require('../utils/checkLive.js');

module.exports = {
  time: 15000,
  async execute(client) {
    const name = 'askemys';
    execute(client, name, {
      color: '#00AD03',
      title: '{{title}}',
      url: `https://www.twitch.tv/${name}`,
      fields: [
        { name: '{{user_name}} est en live sur :', value: '{{game_name}}', inline: true },
      ],
      author: {
        name: '{{user_name}}',
        iconUrl: '{{profile_image_url}}',
        url: `https://www.twitch.tv/${name}`,
      },
      thumbnail: '{{thumbnail_url}}',
    });
  }
}