const ready = require('../events/ready.js');
const { getFollowers } = require('../utils/twitch.js');
const { formatNumber } = require('../utils/numbers.js');
const { nironFollowersId, askemysFollowersId, twitchNironId, twitchAskemysId, guildId, memberCountId } = require('../config.json');

module.exports = {
  delayed: true,
  time: 15000,
  async execute(client) {
    const guild = client.guilds.cache.get(guildId);

    // Get niron and askemys twitch infos
    const nironFollowers = await getFollowers(twitchNironId);
    const askemysFollowers = await getFollowers(twitchAskemysId);

    // Refresh the stats channels
    const memberCountChannel = await guild.channels.fetch(memberCountId);
    const members = await guild.members.fetch();
    const memberCountMessage = `Membres : ${formatNumber(members.size - 1)}`;
    await memberCountChannel.setName(memberCountMessage);

    const nironFollowersChannel = await guild.channels.fetch(nironFollowersId);
    const askemysFollowersChannel = await guild.channels.fetch(askemysFollowersId);
    const nironFollowersMessage = `Follow Niron : ${formatNumber(nironFollowers.total)}`;
    const askemysFollowersMessage = `Follow Askemys : ${formatNumber(askemysFollowers.total)}`;
    await nironFollowersChannel.setName(nironFollowersMessage);
    await askemysFollowersChannel.setName(askemysFollowersMessage);
  }
}