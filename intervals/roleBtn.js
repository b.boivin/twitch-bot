const ready = require('../events/ready.js');

module.exports = {
  delayed: true,
  time: 15000,
  async execute(client) {
    ready.execute(client);
  }
}