const { SlashCommandBuilder } = require("discord.js");
const { roles } = require("../config.json");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("selectrole")
    .setDescription("update command for roles"),
  update: true,
  async execute(interaction) {
    const member = interaction.member;
    await interaction.deferUpdate();
    for (const role of Object.keys(roles).filter(it => it !== 'member')) {
      if (interaction.values.includes(role)) {
        if (!member.roles.cache.has(roles[role])) {
          await member.roles.add(roles[role]);
        }
      } else {
        if (member.roles.cache.has(roles[role])) {
          await member.roles.remove(roles[role]);
        }
      }
    }
    if (!member.roles.cache.has(roles["member"])) {
      await member.roles.add(roles["member"]);
    }
    await interaction.editReply({
      content: "Vos roles ont bien été mis à jour!",
      components: [],
    });
  },
};

