const { SlashCommandBuilder } = require("discord.js");
const fs = require("node:fs");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("rename")
    .setDescription("Renommer le salon dans lequel vous êtes.")
    .addStringOption((option) =>
      option.setName("name").setDescription("Le nouveau nom du salon.").setRequired(true)
    ),
  async execute(interaction) {
    console.log('rename.js')
    const member = interaction.member;
    const channel = member.voice.channel;

    // Get all custom channels
    let voice = [];
    if (fs.existsSync(`./json/dynamicvoice.json`)) {
      const savedJson = fs.readFileSync(`./json/dynamicvoice.json`, 'utf8');
      voice = JSON.parse(savedJson);
    }

    // Check if the channel is a custom channel
    if (!channel) {
      console.log('!channel')
      await interaction.reply({ content: "Vous devez être dans un salon vocal pour utiliser cette commande.", ephemeral: true });
      return;
    } else if (!voice.includes(channel.id)) {
      console.log('!voice.includes(channel.id)')
      await interaction.reply({ content: "Vous devez être dans un salon personnalisé pour utiliser cette commande.", ephemeral: true });
      return;
    }

    console.log('valid')
    // Check if the name is valid
    const name = interaction.options.getString("name");
    await member.voice.channel.setName(name);
    console.log('valid2')
    await interaction.reply({ content: `Le salon a été renommé en \`${name}\``, ephemeral: true });
  },
};
