const { ActionRowBuilder, SelectMenuBuilder, SlashCommandBuilder, StringSelectMenuBuilder } = require("discord.js");
const { roles } = require("../config.json");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("follow")
    .setDescription("Choisir les streamer que tu follow."),
  async execute(interaction) {
    const member = interaction.member;
    console.log(member.guild.emojis.cache.get("1038622935671373864"));
    const row = new ActionRowBuilder().addComponents(
      new StringSelectMenuBuilder()
        .setCustomId("selectrole")
        .setPlaceholder("Choisir un ou plusieurs streamers")
        .setMinValues(1)
        .setMaxValues(2)
        .addOptions([
          {
            label: "Niron",
            value: "niron",
            emoji: member.guild.emojis.cache.get("1038622935671373864").toString(),
            default: member.roles.cache.has(roles.niron),
          },
          {
            label: "Askemys",
            value: "askemys",
            emoji: member.guild.emojis.cache.get("1038622882005266512").toString(),
            default: member.roles.cache.has(roles.askemys),
          },
        ])
    );
    await interaction.reply({ components: [row], ephemeral: true });
  },
};
